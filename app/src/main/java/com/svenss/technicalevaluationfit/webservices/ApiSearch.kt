package com.svenss.technicalevaluationfit.webservices

import com.svenss.technicalevaluationfit.model.ModelResponseSeries
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiSearch {




    @GET("/search/shows?q=?")
    fun searchData(@Query("q")id:String):Call<List<ModelResponseSeries>>


    companion object{

        fun createClientSearch():ApiSearch{

        val retrofit = Retrofit.Builder()
            .baseUrl("http://api.tvmaze.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

            return retrofit.create(ApiSearch::class.java)

            }

    }
}