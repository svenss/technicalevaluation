package com.svenss.technicalevaluationfit.webservices

import com.svenss.technicalevaluationfit.model.ModelResponseLetter
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface ApiMusic {



    @GET("/v1/{artist}/{title}")
    fun sendData(@Path ("artist")artist:String,@Path("title")title:String):Call<ModelResponseLetter>

     companion object{

         fun createClient():ApiMusic{

             val retrofit = Retrofit.Builder()
                 .baseUrl("http://api.lyrics.ovh/")
                 .addConverterFactory(GsonConverterFactory.create())
                 .build()

             return retrofit.create(ApiMusic::class.java)

         }

     }


}