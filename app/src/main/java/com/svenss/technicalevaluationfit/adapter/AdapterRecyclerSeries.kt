package com.svenss.technicalevaluationfit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.svenss.technicalevaluationfit.model.ModelResponseSeries
import kotlinx.android.synthetic.main.adapter_serie_search.view.*

class AdapterRecyclerSeries(
    private var url: List<ModelResponseSeries>,
    private var layoutSearch: Int,
    private var listener: OnItemClickListener
) : RecyclerView.Adapter<AdapterRecyclerSeries.ViewHolder>() {

    init {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutSearch, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return url.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(url, listener)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {



        private var ivSearchCatalogue = view.iv_serie_one


        fun bind(
            url: List<ModelResponseSeries>,
            listener: OnItemClickListener
        ) {

            try {

                Glide.with(itemView.context)
                    .load(url[adapterPosition].show.image.medium)
                    .into(ivSearchCatalogue)

                ivSearchCatalogue.setOnClickListener {
                    try {
                        listener.onClick(adapterPosition)
                    }catch (a: java.lang.NullPointerException){

                        print("Error: $a")
                    }



                }

             }catch (e:java.lang.NullPointerException){

           print(e )

        }
            }

    }

    interface OnItemClickListener {
        fun onClick(position: Int) {

        }

    }


}