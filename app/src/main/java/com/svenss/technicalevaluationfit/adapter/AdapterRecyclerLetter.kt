package com.svenss.technicalevaluationfit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.svenss.technicalevaluationfit.model.ModelResponseLetter
import kotlinx.android.synthetic.main.adapter_music_recycler.view.*

class AdapterRecyclerLetter(
               var response: List<ModelResponseLetter>,
               var layout: Int
                           ) : RecyclerView.Adapter<AdapterRecyclerLetter.ViewHolder>() {

    init {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return response.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(response)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var tvDescription = view.tv_letter_music_service


        fun bind(
            response: List<ModelResponseLetter>
        ) {
                tvDescription.text = response[adapterPosition].lyrics

        }
    }
}
