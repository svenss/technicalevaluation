package com.svenss.technicalevaluationfit.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_recycler_adpter_detail.view.*

class AdapterRecyclerDetailSerie(
    private var url: String,
    private var image:String,
    private var name:String,
    private var chain:String,
    private var layoutSearch: Int
) : RecyclerView.Adapter<AdapterRecyclerDetailSerie.ViewHolder>() {

    init {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layoutSearch, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return url.length
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(url,image,name,chain)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private var ivDetailSerie = view.tv_description_detail
        private var imageDetail = view.iv_detail_serie
        private var tvNameDetail = view.tv_detail_serie
        private var tvChainDetail = view.tv_cadena_detail

        fun bind(
            url: String,
            image:String,
            name: String,
            chain: String
        ) {
            ivDetailSerie.text = url
            tvNameDetail.text = name
            tvChainDetail.text = chain


            Glide.with(itemView.context)
                .load(image)
                .into(imageDetail)

        }
    }


}