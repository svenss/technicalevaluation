package com.svenss.technicalevaluationfit.model

data class Links(
    val previousepisode: Previousepisode,
    val self: Self
)