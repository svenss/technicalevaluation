package com.svenss.technicalevaluationfit.model

data class ModelResponseSeries(
    val score: Double,
    val show: Show
)