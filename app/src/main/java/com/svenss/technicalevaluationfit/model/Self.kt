package com.svenss.technicalevaluationfit.model

data class Self(
    val href: String
)