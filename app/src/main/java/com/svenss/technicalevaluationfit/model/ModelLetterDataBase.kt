package com.svenss.technicalevaluationfit.model

import io.realm.RealmObject

open class ModelLetterDataBase(open var letter:String):RealmObject(){
    constructor():this("Steven")
}