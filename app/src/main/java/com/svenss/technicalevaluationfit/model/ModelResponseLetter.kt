package com.svenss.technicalevaluationfit.model

data class ModelResponseLetter(
    val lyrics: String
)