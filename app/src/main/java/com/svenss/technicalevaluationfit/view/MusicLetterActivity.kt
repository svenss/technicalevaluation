package com.svenss.technicalevaluationfit.view

import android.app.ProgressDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.svenss.technicalevaluationfit.R
import com.svenss.technicalevaluationfit.adapter.AdapterRecyclerLetter
import com.svenss.technicalevaluationfit.model.ModelLetterDataBase
import com.svenss.technicalevaluationfit.model.ModelResponseLetter
import com.svenss.technicalevaluationfit.webservices.ApiMusic
import retrofit2.Call
import retrofit2.Response
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_music_letter.*


class MusicLetterActivity : AppCompatActivity() {

    companion object {
        var persistanceData = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_letter)
        Realm.init(this)

        val titlePresistance = intent.getStringExtra("Title")
        val artist = intent.getStringExtra("Artist")

        connectService(titlePresistance, artist)

    }

    private fun connectService(title: String, artist: String) {

        var recyclerView: RecyclerView?
        var adapter: RecyclerView.Adapter<*>
        var layoutManager: RecyclerView.LayoutManager

        val instanceService = ApiMusic.createClient()
        val call = instanceService.sendData(artist, title)

        val progressDialog = ProgressDialog(this@MusicLetterActivity)

        progressDialog.setMessage("Cargando....")
        progressDialog.setTitle("Fit Fighters")
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        progressDialog.show()

        call.enqueue(object : retrofit2.Callback<ModelResponseLetter> {

            override fun onFailure(call: Call<ModelResponseLetter>, t: Throwable) {
                Toast.makeText(
                    this@MusicLetterActivity,
                    "Fail to conection " + t.localizedMessage,
                    Toast.LENGTH_SHORT
                )
                    .show()
            }

            override fun onResponse(call: Call<ModelResponseLetter>, response: Response<ModelResponseLetter>) {

                response.body()

                if (response.code() == 404) {

                    Toast.makeText(applicationContext, "No se encontraron resultados", Toast.LENGTH_SHORT).show()


                } else {

                    val modelLetter: List<ModelResponseLetter>

                    val parsingModel = response.body()!!

                    modelLetter = listOf(parsingModel)

                    recyclerView = findViewById(R.id.recyclerview_letter_music)
                    layoutManager = LinearLayoutManager(this@MusicLetterActivity)


                    adapter = AdapterRecyclerLetter(
                        modelLetter,
                        R.layout.adapter_music_recycler
                    )

                    recyclerView!!.adapter = adapter
                    recyclerView!!.layoutManager = layoutManager


                    persistanceData = modelLetter[0].lyrics

                    icon_add_letter.isVisible = true
                }
                progressDialog.dismiss()


            }
        })

        print(persistanceData)

        icon_add_letter.isVisible = false

        icon_add_letter.setOnClickListener {


            persistanceDatas()

        }
    }

    fun persistanceDatas() {

        val letterNew = ModelLetterDataBase(persistanceData)
        val realm: Realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insert(letterNew)
        realm.commitTransaction()
        realm.close()
        Toast.makeText(this, "Se agrego correctamente", Toast.LENGTH_SHORT).show()

    }
}
