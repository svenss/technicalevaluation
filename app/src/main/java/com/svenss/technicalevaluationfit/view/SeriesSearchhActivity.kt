package com.svenss.technicalevaluationfit.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.svenss.technicalevaluationfit.R
import com.svenss.technicalevaluationfit.adapter.AdapterRecyclerSeries
import com.svenss.technicalevaluationfit.controller.FragmentSearchSeries
import com.svenss.technicalevaluationfit.model.ModelResponseSeries
import com.svenss.technicalevaluationfit.webservices.ApiSearch
import kotlinx.android.synthetic.main.activity_series_searchh.*
import retrofit2.Call
import retrofit2.Response

class SeriesSearchhActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_series_searchh)

        var id:String

        iv_search_serie_menu.setOnClickListener {

            val textFragment = FragmentSearchSeries()
            val manager = supportFragmentManager
            val transaction = manager.beginTransaction()

            transaction.replace(R.id.fragment_container, textFragment)
            transaction.addToBackStack(null)

            transaction.commit()

             id = seach_serie_menu.text.toString()

            searchSerieService(id)
        }
    }


    private fun searchSerieService(id:String) {

        val getDataSearch = ApiSearch.createClientSearch()
        val call = getDataSearch.searchData(id)

        var recyclerView: RecyclerView?
        var adapter: RecyclerView.Adapter<*>
        var layoutManager: RecyclerView.LayoutManager

        call.enqueue(object : retrofit2.Callback<List<ModelResponseSeries>> {

            override fun onFailure(call: Call<List<ModelResponseSeries>>, t: Throwable) {
                print(t.localizedMessage)
            }

            override fun onResponse(
                call: Call<List<ModelResponseSeries>>,
                response: Response<List<ModelResponseSeries>>
            ) {

                try {

                    val respuesta: List<ModelResponseSeries>

                    val url = response.body()!!

                    respuesta = url

                    if(url.isNotEmpty()) {

                        recyclerView = findViewById(R.id.recycler_fragment)
                        layoutManager = GridLayoutManager(this@SeriesSearchhActivity, 3)


                        adapter = AdapterRecyclerSeries(
                            respuesta,
                            R.layout.adapter_serie_search,
                            object : AdapterRecyclerSeries.OnItemClickListener {
                                override fun onClick(position: Int) {
                                    val intent = Intent(this@SeriesSearchhActivity, DetailSerieActivity::class.java)
                                    val bundle = Bundle()
                                    bundle.putString("Image", respuesta[position].show.image.original)
                                    bundle.putString("Description", respuesta[position].show.summary)
                                    bundle.putString("Chain", respuesta[position].show.network.name)
                                    bundle.putString("Name", respuesta[position].show.name)
                                    intent.putExtras(bundle)
                                    startActivity(intent)
                                }
                            }
                        )

                        recyclerView!!.adapter = adapter
                        recyclerView!!.layoutManager = layoutManager
                    }else{
                        Toast.makeText(applicationContext, "No hay resultados, intenta nuevamente..", Toast.LENGTH_SHORT).show()
                    }


                }catch (e:java.lang.NullPointerException){

                    Toast.makeText(applicationContext, "No hay resultados, intenta nuevamente..", Toast.LENGTH_SHORT).show()

                }
            }
        })
    }
}
