package com.svenss.technicalevaluationfit.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.svenss.technicalevaluationfit.R
import kotlinx.android.synthetic.main.activity_music_search.*


class MusicSearchActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_search)

        var title:String
        var artist:String

        btn_search_letter.setOnClickListener {

            title = et_title_search.text.toString()
            artist = et_artist_search.text.toString()

            if (title == "" || artist == ""){

                Toast.makeText(applicationContext, "Campo vacio, por favor ingrese un Titulo y Artista", Toast.LENGTH_SHORT).show()

              }else {

                 searchData(title, artist)
            }

        }

        iv_favorite_music.setOnClickListener {

            val intent = Intent(this, ListLetterActivity::class.java)
            startActivity(intent)
        }
    }

    fun searchData(title:String, artist:String){

        val intent = Intent(this@MusicSearchActivity, MusicLetterActivity::class.java)
        val bundle = Bundle()
        bundle.putString("Title", title.toLowerCase())
        bundle.putString("Artist", artist.toLowerCase())
        intent.putExtras(bundle)
        startActivity(intent)

    }
}
