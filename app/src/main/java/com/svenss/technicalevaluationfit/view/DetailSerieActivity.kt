package com.svenss.technicalevaluationfit.view

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.svenss.technicalevaluationfit.R
import com.svenss.technicalevaluationfit.adapter.AdapterRecyclerDetailSerie
import com.svenss.technicalevaluationfit.adapter.AdapterRecyclerLetter
import kotlinx.android.synthetic.main.layout_recycler_adpter_detail.*


class DetailSerieActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_serie)

        val recyclerView: RecyclerView?
        val adapter: RecyclerView.Adapter<*>
        val layoutManager: RecyclerView.LayoutManager


        val imageDetail = intent.getStringExtra("Image")
        val descriptionDetail = intent.getStringExtra("Description")
        val chainDetail = intent.getStringExtra("Chain")
        val nameDetail = intent.getStringExtra("Name")

        recyclerView = findViewById(R.id.recycler_detail_serie)
        layoutManager = LinearLayoutManager(this@DetailSerieActivity)

        adapter = AdapterRecyclerDetailSerie(
            descriptionDetail,
            imageDetail,
            nameDetail,
            chainDetail,
            R.layout.layout_recycler_adpter_detail
        )

        recyclerView!!.adapter = adapter
        recyclerView!!.layoutManager = layoutManager





    }
}
