package com.svenss.technicalevaluationfit.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.svenss.technicalevaluationfit.R
import com.svenss.technicalevaluationfit.controller.DataBaseLetterActivity
import com.svenss.technicalevaluationfit.model.ModelLetterDataBase
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_list_letter.*

class ListLetterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_letter)


        tv_list_coldplay.setOnClickListener {


            val intent = Intent(this, DataBaseLetterActivity::class.java)
            startActivity(intent)
        }

    }
}
