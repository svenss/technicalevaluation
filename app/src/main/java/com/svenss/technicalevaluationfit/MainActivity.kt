package com.svenss.technicalevaluationfit

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.svenss.technicalevaluationfit.controller.FragmentSearchSeries
import com.svenss.technicalevaluationfit.view.ListLetterActivity
import com.svenss.technicalevaluationfit.view.MusicLetterActivity
import com.svenss.technicalevaluationfit.view.MusicSearchActivity
import com.svenss.technicalevaluationfit.view.SeriesSearchhActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_proUno.setOnClickListener {

            val intent = Intent(this, MusicSearchActivity::class.java)
            startActivity(intent)

        }

        tv_proDos.setOnClickListener {

            val intent = Intent(this, SeriesSearchhActivity::class.java)
            startActivity(intent)

        }
    }
}
