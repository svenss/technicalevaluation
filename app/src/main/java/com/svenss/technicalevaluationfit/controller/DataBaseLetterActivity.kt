package com.svenss.technicalevaluationfit.controller

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.svenss.technicalevaluationfit.R
import com.svenss.technicalevaluationfit.adapter.AdapterRecyclerDataBase
import com.svenss.technicalevaluationfit.model.ModelLetterDataBase
import com.svenss.technicalevaluationfit.view.ListLetterActivity
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_data_base_letter.*

class DataBaseLetterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_base_letter)

        val response: String
        var realm: Realm

        try {

            realm = Realm.getDefaultInstance()
            realm.beginTransaction()

            val  prueba = realm.where(ModelLetterDataBase::class.java).findAll()
            response = prueba[0]!!.letter
            realm.commitTransaction()
            realm.close()

            parsingData(response)

        } catch (q: java.lang.RuntimeException) {

            Toast.makeText(this, "No hay letras almacenadas..", Toast.LENGTH_SHORT).show()
            icon_delete_letter.isVisible = false

        }

        icon_delete_letter.setOnClickListener {

            realm = Realm.getDefaultInstance()
            realm.beginTransaction()

            val  prueba = realm.where(ModelLetterDataBase::class.java).findAll()
            prueba.deleteAllFromRealm()
            realm.commitTransaction()
            realm.close()
            Toast.makeText(this, "Se eliminaron los datos correctamente", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, ListLetterActivity::class.java)
            startActivity(intent)

        }
    }

    private fun parsingData(response: String) {

        val recyclerView: RecyclerView? = recycler_data
        val adapter: RecyclerView.Adapter<*>
        val layoutManager: RecyclerView.LayoutManager

        layoutManager = LinearLayoutManager(this)
        adapter = AdapterRecyclerDataBase(
            response,
            R.layout.adapter_music_recycler
        )
        recyclerView!!.adapter = adapter
        recyclerView.layoutManager = layoutManager
    }
}
